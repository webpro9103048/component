import { ref } from 'vue'
import { defineStore } from 'pinia'
interface AddressBook {
  id: number
  name: string
  tel: string
  gender: string
}
export const useaddressBookStore = defineStore('adreress_book', () => {
  let lastId = 1
  const address = ref<AddressBook>({
    id: 0,
    name: '',
    tel: '',
    gender: 'male'
  })
  const addressList = ref<AddressBook[]>([])
  const isAddnew = ref(false)
  function save() {
    if(address.value.id>0){ // Edit
      const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
      addressList.value[editedIndex] = address.value
    }else{ //Add new
      addressList.value.push( {...address.value, id: lastId++ })
    }
    isAddnew.value = false
    address.value = {
    id: 0,
    name: '',
    tel: '',
    gender: 'male'
  }
  }
  
  function edit(id: number) {
    isAddnew.value = true
    const editedIndex = addressList.value.findIndex((item) => item.id === id) 
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
  }
  function remove(id: number) {
    const removedIndex = addressList.value.findIndex((item) => item.id === id) 
    address.value = JSON.parse(JSON.stringify(addressList.value[removedIndex]))
    addressList.value.splice(removedIndex, 1)
  }
  function cancel() {
    isAddnew.value = false
  
    address.value = {
    id: 0,
    name: '',
    tel: '',
    gender: 'male'
  }
  }

  return { address, addressList, save, isAddnew, edit, remove, cancel }
})
